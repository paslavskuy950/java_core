package com.company.lesson1_types_mathOperations_if;

public class Classwork {

    public static void main(String[] args) {
	    System.out.println("Hello world!");

	    int a = 5;
	    double b = 5.6;
	    boolean c = false; //false або true
        char d = 'a';
        String name = "Igor";

        int numberOfApples = 4;
        int numbersOfOranges = 3;
        double numbersOf = 3;

        int numberOfFructs = numberOfApples + numbersOfOranges;
        int x = numberOfApples - numbersOfOranges;
        int y = numberOfApples * numbersOfOranges;
        double z = numberOfApples / numbersOf;

        System.out.println(numberOfFructs);
        System.out.println(x);
        System.out.println(y);
        System.out.println(z);

        int num = 5;
        int ostacha = 5 % 3;

        System.out.println(ostacha);

        ++num;

        System.out.println(num);

        --num;

        System.out.println(num);

        num = num * 2; //10
        num *= 2;
        num += 2;
        num -= 2;
        num /= 2;
        num %= 2;

        int value = 5;
        value = 10;
        value = value + num;

        int a1 = 10;
        int b1 = 5;
        int square;

        square = a1*b1;

        System.out.println(square);

        int xyz = 5;

        if (xyz == 6) {
            System.out.println("Equals1");
        }

        if (xyz != 6) {
            System.out.println("Equals2");
        }

        if (xyz > 6) {
            System.out.println("Equals3");
        }

        if (xyz < 6) {
            System.out.println("Equals4");
        }

        if (xyz >= 5) {
            System.out.println("Equals5");
        }

        if (xyz <= 5) {
            System.out.println("Equals6");
        }

        if (xyz < 5 || xyz > 10) {
            System.out.println("ABO");
        }

        if (xyz > 10) {
            System.out.println("Bigger");
        } else {
            System.out.println("not bigger");
        }

        if (xyz > 10) {
            System.out.println("Bigger");
        } else if (xyz > 8){
            System.out.println("not bigger than 8");
        } else {
            System.out.println("not bigger");
        }
    }
}
