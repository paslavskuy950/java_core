package com.company.lesson3_methods_cycles;

import java.util.Scanner;

public class Classwork2 {
    public static void main(String[] args) {
//        int i = 0;
//
//        while (true) {
//
//            i = i + 5;
//
//            if (i == 5) {
//                break;
//            }
//
//            System.out.println(i);
//            ++i;
//
//            if (i < 3) {
//                continue;
//            }
//
//            System.out.println("Go");
//        }
//
//        int a = 0;
//
//        Scanner scanner = new Scanner(System.in);
//
//        while (true) {
//            System.out.println("Enter a: ");
//            a = scanner.nextInt();
//
//            if (a < 10) {
//                continue;
//            }
//
//            System.out.println(a);
//
//            if (a > 20) {
//                break;
//            }
//        }

        int v1 = 10;
        int v2 = 15;

        int sum = sum(v1, v2);
        System.out.println(sum);
        System.out.println(v1);

        print("");

        System.out.println(max(10, 5));

        int max = 0;

        print(max);
        double x = 0;
        print(x);

        short y = 0;

        print(y);

        int z = y;

        long l = 0L;
        print(l);
    }

    public static void print(int i) {
        System.out.println("INT");
    }

    public static int print(int i, double x) {
        System.out.println("INT");
        return 5;
    }

    public static void print(int i, int z) {
        System.out.println("INT 2");
    }

    public static void print(double i) {
        System.out.println("DOUBLE");
    }

    public static void print(String text) {
        if (text.equals("")) {
            return;
        }

        System.out.println(text);
    }

    public static int sum(int a, int b) {
        a = 5;
        System.out.println(a);
        return a + b;
    }




    public static int max(int a, int b) {
        if (a > b) {
            return a;
        } else {
            return b;
        }
    }

    public static int v(int a, int b, int c) {
        return a * b * c;
    }

    public static boolean divideOn5(int a) {
        if (a % 5 == 0) {
            return true;
        }

        return false;
    }

    public static boolean divideOn5Version2(int a) {
        return a % 5 == 0;
    }

    public static boolean isDivideable(int a, int b) {
        return a % b == 0;
    }
}
