package com.company.lesson3_methods_cycles;

public class Classwork {
    public static void main(String[] args) {

//        int i = 0;
//
//        while (i < 5) {
//            System.out.println(i);
//            ++i;
//        }

//        for (int i = 0; i < 5; i += 2) {
//            System.out.println(i);
//        }

//        int i = 0;
//
//        do {
//            System.out.println(i);
//            ++i;
//        } while (i < 5);

        printText();
        double a = getValue();
        System.out.println(a);

        double value = 10;

        double sq = sqr(value);
        System.out.println(sq);

        //Task 1
        int n = 1;

        for (int i = 0; i < 55; ++i) {
            System.out.println(n);
            n += 2;
        }

        //Task 2
        boolean isEven = isEven(10);
        System.out.println(isEven);
    }

    public static void printText() {
        System.out.println("Print");
    }

    public static double getValue() {
        return 5;
    }

    public static double sqr(double x) {
        return x * x;
    }

    //Task 2
    public static boolean isEven(int x) {
        return x % 2 == 0;
    }
}
