package com.company.lesson2_specificTypes_scanner_switch_string;

import java.util.Scanner;

public class Calculator {

    public static void main(String[] args) {

        double a = 0;
        double b = 0;
        double result = 0;

        String operation;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter a: ");

        if (scanner.hasNextDouble()) {
            a = scanner.nextDouble();
        } else {
            scanner.next();
        }

        System.out.println("Enter b: ");

        if (scanner.hasNextDouble()) {
            b = scanner.nextDouble();
        } else {
            scanner.next();
        }

        operation = scanner.next();

        switch (operation) {
            case "+": {
                result = a + b;
                System.out.println(result);
            } break;
            case "-": {
                result = a - b;
                System.out.println(result);
            } break;
            case "*": {
                result = a * b;
                System.out.println(result);
            } break;
            case "/": {
                if (b == 0) {
                    System.out.println("We can't divide on 0");
                } else {
                    result = a / b;
                    System.out.println(result);
                }
            } break;
            default: {
                System.out.println("Wrong operation");
            }
        }
    }
}
