package com.company.lesson2_specificTypes_scanner_switch_string;


import java.util.Scanner;

public class Classwork {
    public static void main(String[] args) {

        // Цілі числа
        byte b = 55; // 1 byte
        short s = 5555; // 2 byte
        int i = 555555555; // 4 byte
        Long l = 5555555555555555555L; // 8 byte

        // Дробові числа
        double d = 5.5; // 8 byte
        float f = 5.5f; // 4 byte

        // Символьні типи
        String str = "text"; // текст
        char c = 'a'; // 2 byte
        char c2 = 80;

        boolean isPaid = true;

        System.out.println(c2);

        Math.sqrt(5);

        if (true || false) {
            System.out.println("true");
        }

        if (true && !false) {
            System.out.println();
        }

        if (isPaid) {
            System.out.println("Вам включать світло");
        }

        Scanner scanner = new Scanner(System.in);

//        System.out.println("Введіть а");
//        int a = scanner.nextInt();
//        System.out.println("a = " + a);
//        double d2 = scanner.nextDouble();

        System.out.println("Enter number of apples: ");
        int numberOfApples = 5;
//        int numberOfApples = scanner.nextInt();

        String text = "Igor had " + numberOfApples + " apples";
        System.out.println(text);
        text += " and 2 oranges";
        System.out.println(text);

        String text2 = "abc ";

        if (text2.equals("abc")) {
            System.out.println("equals");
        }

        if (text2.equalsIgnoreCase("Abc")) {
            System.out.println("equals");
        }

        System.out.println(text2.length()); // Довжина стрічки

//        final String winter = "winter";
//        final String spring = "spring";
//        final String summer = "summer";
//        final String autumn = "autumn";

//        System.out.println("Enter season: ");
        String season = "sd";
//        String season = scanner.next();

//        if (season.equals(winter)) {
//            System.out.println("snow");
//        } else if (season.equals(spring)) {
//            System.out.println("Green");
//        } else if (season.equals(summer)) {
//            System.out.println("Sun");
//        } else if (season.equals(autumn)){
//            System.out.println("Yellow");
//        } else {
//            System.out.println("Error");
//        }

//        switch (season) {
//            case winter: {
//                System.out.println("snow");
//                break;
//            }
//            case spring: {
//                System.out.println("green");
//                break;
//            }
//            case summer: {
//                System.out.println("sun");
//                break;
//            }
//            case autumn: {
//                System.out.println("yellow");
//                break;
//            }
//            default: {
//                System.out.println("default");
//            }

        System.out.println("Enter a: ");
        double value1 = 0;

        if (scanner.hasNextDouble()) {
            value1 = scanner.nextDouble();
        } else {
            scanner.next();
        }
    }
}
