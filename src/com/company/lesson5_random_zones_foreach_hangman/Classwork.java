package com.company.lesson5_random_zones_foreach_hangman;

import java.util.Random;

public class Classwork {
    public static void main(String[] args) {
        String[] seasons = {"winter", "spring", "summer", "autumn"};

        int a = 10;

        for (String season : seasons) {
            if (true) {
                System.out.println(a);
            }
            System.out.println(a);
            System.out.println(season);
            season = "abc";
        }

        for (String season : seasons) {
            System.out.println(season);
        }

        int x = 5;
        System.out.println(x);

        m(x);
        System.out.println(x);

        Random random = new Random();

        int min = 50;
        int max = 151;
        int diff = max - min;

        System.out.println("random");

//        for (int i = 0; i < 100; ++i) {
//            System.out.println(random.nextInt(diff) + min);
//        }

        int[] array = new int[10];

        for (int i = 0; i < array.length; ++ i) {
            array[i] = random.nextInt(diff) + min;
        }

        for (int elem : array) {
            System.out.println(elem);
        }
    }

    public static void m(int a) {
        System.out.println(a);
    }
}
